\section{Methodology}

Notations are firstly defined as in Table \ref{tableSymbol}. Let $m$ denotes the number of modalities, $l$ denotes number of autoencoder layers, $n$ denotes the middle layer, $S$ denotes the set of triplets in knowledge graph, $h, r, t$ represents head entity, relation and tail entity respectively. For layer $j$, $d_i^{(j)}, W_i^{(j)}$ and $b_i^{(j)}$ denote the representation, weight matrix and bias for modality $i$ respectively. $L$ is the total loss, consisting of $L_a$, the autoencoder loss, $L_e$, the structure loss and $\Omega(\theta)$, the regularizer.


\subsection{Overall Structure}
\input{figOverall}

To learn from multimodal knowledge and structural knowledge jointly,
we first get knowledge from each single modality by learning feature vectors for each modality respectively,
Then we feed these feature vectors into multimodal autoencoder.
For entities, we let the middle-hidden layer of autoencoder satisfy the TransE assumption with corresponding relations.
The overall architecture of TransAE is shown in Figure \ref{overall}.

\subsection{Modal Knowledge Representation}
We use multimodal autoencoder to learn knowledge of different modalities.
Multimodal autoencoder is a feedforward neural network consisting of an input layer,
multiple hidden layers and a output layer.
The input layer, also the first layer of the autoencoder,
takes input $d_i^{(1)}(i = 1,\dots, m)$ from $m$ different modalities,
which are feature vectors extracted from descriptions of all these modalities.
A fully-connected hidden layer with much less units is followed the input layer for each modality,
we map the input to these hidden layers seperately,
denoting as $d_i^{(n)}(n = 1, \dots, l)$,
for representation of modality $i$ in layer $n$.
The middle hidden layer $d^{(n)}$ is jointly mapped from the previous layer $d_i{(n-1)}$,
calculated as 
\begin{align}
	d^{n} = f(W^{(n-1)} \times (d_{1}^{(n-1)} \oplus d_{2}^{(n-1)} \oplus \cdots \oplus d_m^{(n-1)}) + b^{(n-1)})
\end{align}
where symbol $\oplus$ represents the concatenate operation,
$W$ denotes the weight matrix for this layer,
$f$ is an activation function,
and $d^{(n)}$ serves as the desired joint multimodal entity representation.

The decoder stage has exactly symmetrical structure with the encoder part,
taking embedding $d^{(n)}$ as input,
first map it to $m$ different hidden layers seperately,
each shares the same dimension with the corresponding hidden layer in encoder,
Output layer and input layer have the same dimension for each modality.
Output layer is also called ``reconstruction'' layer, 
the goal of multimodal autoencoder is to reconstruct the input feature vectors in the reconstruction layer.

The whole architecture of multimodal autoencoder is defined as:

\begin{comment}
\begin{align}
&\imageLatent = f(\weightsImgLatent \times \imageInput + \biasImgLatent) \\
&\textLatent = f(\weightsTxtLatent \times \textInput + \biasTxtLatent) \\
&\jointLatent = f(\weightsLatent \times (\imageLatent \oplus \textLatent) + \biasLatent)\\
&\imageLatentDecoder = f(\weightsImgLatentDecoder \times \jointLatent + \biasImgLatentDecoder) \\
&\textLatentDecoder = f(\weightsTxtLatentDecoder \times \jointLatent + \biasTxtLatentDecoder) \\
&\imageOutput = f(\weightsImgOutput \times \imageLatentDecoder + \biasImgOutput) \\
&\textOutput = f(\weightsTxtOutput \times \textLatentDecoder + \biasTxtOutput)
\end{align}
\end{comment}


\begin{align}
d_i^j = f(W_i^{(j-1)} &\times d_i^{(j-1)} + b_i^{(j-1)}) \\
d^{n} = f(W^{(n-1)} &\times (d_{1}^{(n-1)} \oplus d_{2}^{(n-1)} \oplus \cdots \oplus d_m^{(n-1)}) + b^{(n-1)}) \\
s.t. \qquad \quad i,j &\in N;  \\
1 \leq &i \leq m; \\
2 \leq &j \leq l, j \neq n
\end{align}

\noindent $W_i^{n}$ denotes the weight matrix for modality $i$ in layer $n$,
$d_i^{(n)}$ is the representation of modality $i$ in layer $n$,
and $b_i^{(n)}$ represents the bias for modality $i$ in layer $n$.

The whole multimodal autoencoder is trained to minimize reconstruction loss $\aeloss$,
which is the sum of squared dissimilarities between input and output layers for all modalities:

\begin{align}
\aeloss = \sum_{i=1}^{m} \left\|d_i^{(1)} - d_i^{(l)}\right\|_2^2
\end{align}

\noindent where $m$ is the total amount of modalities and $l$ is the number of layers.

\subsection{Structure Knowledge Representation}
Existing translation-based representation learning methods can capture structure knowledge of entities and relations.
In this paper, we use the most fundamental method TransE\cite{NIPS2013_5071} to learn for structure knowledge.
TransE model maps entities and relations in low-dimensional continuous vector space, 
where embeddings of head entity $\head$, tail entity $\tail$, and relation $\relation$ satisfy:

\begin{align}
\head + \relation \approx \tail
\end{align}

In TransE, we try to minimize the distance between $\head + \relation$ and $\tail$ for triplets in knowledge graph, 
and maximize distance for triplets out of knowledge graph, which are constructed on our own, referred to as ``corrupted'' triplets. 
While training TransE, we minimize loss $\transeloss$:

\begin{equation}
\begin{split}
\transeloss =& \sum_{(\head, \relation, \tail)\in S} \sum_{(\head', \relation', \tail')\in S'}\max(0, [\gamma + \\
&d(\head, \relation, \tail)-d(\head', \relation', \tail')])
\end{split}
\end{equation}


\noindent where $\gamma$ is a margin hyperparameter, and $\max(0, x)$ aims to get the positive part of $x$, $d$ is the dissimilarity function, can either be L1 norm or L2 norm. $S$ denote the original dataset consists of golden triplets existed in knowledge graph, while $S'$ is the corrupted dataset constructed by replacing head or tail entity for each triplet, following:

\begin{align}
S'_{\head, \relation, \tail}={(\corruptedHead, \relation, \tail)|\corruptedHead \in \entitySet } \cup {(\head, \relation, \corruptedTail)|\corruptedTail \in \entitySet }
\end{align}

\subsection{Joint Learning of Multimodal and Structure Knowledge}
In our TransAE framework, we combine the above two models to learn from multimodal knowledge and structural knowledge simultaneously.
After extracting feature vectors for each modality,
we feed these features into multimodal autoencoder to get the joint embedding as the entity representation.
Relation embeddings are initialized randomly in the beginning of the training. 
Structure loss $\transeloss^{'}$ for $(\head, \relation, \tail)$ can be represented as:

\begin{equation}
\begin{split}
\transeloss' =& \sum_{(\head, \relation, \tail)\in S} \sum_{(\head', \relation', \tail')\in S'}\max(0, [\gamma + \\
	& d(d_h^{(n)}, \relation, d_t^{n})-d(d_{h'}^{n}, \relation', d_{t'}^{n})])
\end{split}
\end{equation}

\noindent where $d_h^{n}$ and $d_t^{n}$ represents the representation for $\head$ and $\tail$ embeddings respectively. 
For better generalization, we also add a regularizer item $\regularizer$  for parameter set $\theta$, with $\weightsRegularizer$ as its weight.
Thus we can train our model by minimizing the overall loss $\loss$: 

\begin{align}
L = \aeloss + \weightsaeloss\transeloss' + \weightsRegularizer\regularizer
\end{align}

Parameters $W_i^{(j)}$, $W_t^{(j)}$,  $b_i^{(j)}$, $b_t^{(j)}$ in autoencoder and relation embeddings are initialized randomly at the beginning of training. 
Weight parameters $\weightsaeloss$ and $\weightsRegularizer$ are chosen to balance the magnitude and importance of losses $\aeloss$, $\transeloss'$ and regularizer $\regularizer$.

At each round during training, a set of triplets are randomly selected, combing with the constructed corrputed triplets, together serve as batch data. 
We train our model on the training set and select optimal parameters based on the validation dataset. 
Performance of our model is illustrated in the next section in detail.


